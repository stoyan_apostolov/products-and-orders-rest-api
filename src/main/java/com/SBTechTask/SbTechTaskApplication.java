package com.SBTechTask;

import com.SBTechTask.entities.Orders;
import com.SBTechTask.entities.Products;
import com.SBTechTask.entities.util.OrderStatus;
import com.SBTechTask.repos.OrdersRepository;
import com.SBTechTask.repos.ProductsRepository;
import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class SbTechTaskApplication {

	public static void main(String[] args) {
		BasicConfigurator.configure();
		SpringApplication.run(SbTechTaskApplication.class, args);
	}

	@Bean
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Bean
	CommandLineRunner runner(ProductsRepository productsRepository, OrdersRepository ordersRepository){
		return args -> {

			Products apple = new Products(1, "apple", "fruit", 3);
			Products corn = new Products(2, "corn", "veggie", 5);
			Products pepper = new Products(3, "pepper", "veggie", 2);
			Products pineapple = new Products(4, "pineapple", "fruit", 8);
			Products potato = new Products(5, "potato", "veggie", 1.1);
			Products milk = new Products(6, "milk", "dairy", 20.66);

			productsRepository.save(apple);
			productsRepository.save(corn);
			productsRepository.save(pepper);
			productsRepository.save(pineapple);
			productsRepository.save(potato);
			productsRepository.save(milk);

			List<Long> firstOrders = new ArrayList<>();
			firstOrders.add(apple.getId());
			firstOrders.add(corn.getId());
			firstOrders.add(pepper.getId());

			List<Long> secondOrders = new ArrayList<>();
			secondOrders.add(milk.getId());
			secondOrders.add(pineapple.getId());
			secondOrders.add(potato.getId());

			ordersRepository.save(new Orders(1, new Date(), firstOrders, OrderStatus.PROCESSING));
			ordersRepository.save(new Orders(2, new Date(), secondOrders, OrderStatus.CANCELLED));
		};
	}
}

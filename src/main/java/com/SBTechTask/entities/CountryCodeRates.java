package com.SBTechTask.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountryCodeRates {

    @JsonProperty("country")
    String country;

    @JsonProperty("standard_rate")
    String standardRate;

    @JsonProperty("reduced_rate")
    String reducedRate;

    @JsonProperty("reduced_rate_alt")
    String reduced_rate_alt;

    @JsonProperty("super_reduced_rate")
    String super_reduced_rate;

    @JsonProperty("parking_rate")
    String parking_rate;

    public CountryCodeRates() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStandardRate() {
        return standardRate;
    }

    public void setStandardRate(String standardRate) {
        this.standardRate = standardRate;
    }

    public String getReducedRate() {
        return reducedRate;
    }

    public void setReducedRate(String reducedRate) {
        this.reducedRate = reducedRate;
    }

    public String getReduced_rate_alt() {
        return reduced_rate_alt;
    }

    public void setReduced_rate_alt(String reduced_rate_alt) {
        this.reduced_rate_alt = reduced_rate_alt;
    }

    public String getSuper_reduced_rate() {
        return super_reduced_rate;
    }

    public void setSuper_reduced_rate(String super_reduced_rate) {
        this.super_reduced_rate = super_reduced_rate;
    }

    public String getParking_rate() {
        return parking_rate;
    }

    public void setParking_rate(String parking_rate) {
        this.parking_rate = parking_rate;
    }
}

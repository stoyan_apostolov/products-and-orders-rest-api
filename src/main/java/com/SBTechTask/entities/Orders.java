package com.SBTechTask.entities;

import com.SBTechTask.entities.util.OrderStatus;
import org.hibernate.engine.internal.Cascade;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date date;

    //    @OneToMany(orphanRemoval = true, targetEntity = Products.class)
////    @JoinColumn(name = "PRODUCTS_ID")
//    @JoinTable(name = "ORDERS")
//    @OneToMany(mappedBy = "orders", cascade = CascadeType.ALL, targetEntity = Products.class)
    @ElementCollection(targetClass = Long.class)
    private List<Long> productsIDS;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    public Orders(long id, Date date, List<Long> productsIDS, OrderStatus status) {
        this.id = id;
        this.date = date;
        this.productsIDS = productsIDS;
        this.status = status;
    }

    public Orders(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Long> getProductsIDS() {
        return productsIDS;
    }

    public void setProductsIDS(List<Long> productsIDS) {
        this.productsIDS = productsIDS;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }
}

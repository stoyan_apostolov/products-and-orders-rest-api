package com.SBTechTask.entities;

import java.util.Map;

public class Rates {

    String lastUpdated;
    String disclaimer;
    Map<String, CountryCodeRates> rates;

    public Rates() {
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public Map<String, CountryCodeRates> getRates() {
        return rates;
    }

    public void setRates(Map<String, CountryCodeRates> rates) {
        this.rates = rates;
    }
}

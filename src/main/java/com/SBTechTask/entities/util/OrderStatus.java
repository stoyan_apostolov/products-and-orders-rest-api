package com.SBTechTask.entities.util;

public enum OrderStatus {

    PENDING, PROCESSING, DELIVERED, CANCELLED

}

package com.SBTechTask.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;

import org.apache.log4j.Logger;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtUtil {

    private static final Long TOKEN_EXPIRATION_TIME = 3600000L;
    private static final String SECRET_KEY = "pizza";
    private final static Logger logger = Logger.getLogger(JwtUtil.class);


    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

//    public String extractCountryCode(String token) {
//        Claims claims = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
//        String value = claims.get("countryCode").toString();
//        System.out.printf("This is the value of the shit - %s", value);
//        return value;
//    }

    public String extractCountryCode(String token) {
        String[] splitString = token.split("\\.");
        String base64EncodedBody = splitString[1];
        String body = new String(TextCodec.BASE64.decode(base64EncodedBody));
        return body.substring(body.indexOf("countryCode") + 14, body.indexOf("countryCode") + 16);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("countryCode", "BG");
        //the map is currently filled with only one country code,
        // but can be filled with any claim that has to be included in the payload

        return createToken(claims, userDetails.getUsername());
    }

    private String createToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_EXPIRATION_TIME)).claim("scope", "admins")
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

}

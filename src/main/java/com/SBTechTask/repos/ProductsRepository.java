package com.SBTechTask.repos;

import com.SBTechTask.entities.Products;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "products", path = "products")
public interface ProductsRepository extends PagingAndSortingRepository<Products, Long> {

    //additionally a findBy method could be used to filter the products by category

}

package com.SBTechTask.controller;

import com.SBTechTask.entities.Products;
import com.SBTechTask.entities.Rates;
import com.SBTechTask.models.AuthenticationRequest;
import com.SBTechTask.models.AuthenticationResponse;
import com.SBTechTask.services.MyUserDetailsService;
import com.SBTechTask.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


@RestController
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private RestTemplate restTemplate;

    //the user is gonna post the username and password as body of the request
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(), authenticationRequest.getPassword()));

        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    @RequestMapping(value = "/vatapi", method = RequestMethod.GET)
    public ResponseEntity<?> calculateVat(@RequestBody Products products, @RequestHeader(name = "Authorization") String jwt) {

        String url = "https://euvatrates.com/rates.json";
        Rates rates = restTemplate.getForObject(url, Rates.class);
        String countryCode = jwtTokenUtil.extractCountryCode(jwt);
        double standardRate = Double.parseDouble(rates.getRates().get(countryCode).getStandardRate());
        double addedValue = standardRate / 100 * products.getPrice();

        Products productWithVat = new Products(products.getId(), products.getName(),
                products.getCategory(), products.getPrice() + addedValue);

        return ResponseEntity.ok(productWithVat);
    }

}

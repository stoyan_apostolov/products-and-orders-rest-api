# Products and Orders Rest API

As a task for SBTech I implemented a REST API to manage PRODUCTS and ORDERS with in-memory database, 
using JWT authentication and public VAT API to get country tax rates.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
### Prerequisites

What things you need to install the software and how to install them. 

```
An IDE and an API client of your choice. Clone the project and run it.
```

## Usage

One valid user has been hardcoded with username stoyan and password 1234. 
To generate the token you need to go to the exposed REST endpoint:

```
http://localhost:8080/authenticate
```

And make a POST request with the following JSON body:

```
{
"username" : "stoyan",
"password" :"1234"
}
```

The response should be something like this:

```
{
    "jwt": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdG95YW4iLCJjb3VudHJ5Q29kZSI6IkJHIiwic2NvcGUiOiJhZG1pbnMiLCJleHAiOjE1OTMwMDcwNDIsImlhdCI6MTU5MzAwMzQ0Mn0.-xBJrozUMphYZLigYzj5CR0aE2wAaIPlyWmkvo_6At8"
}
```

With the aforementioned token, the user can pass it as a header of a request and authenticate. 
More specifically the header name should be Authorization and the value should be Bearer and the token string.
 
You have to pass the token as a header to authenticate for each operation, except for retrieving all products
and the database(for dev purposes only)

All CRUD operations available. For example to create a new product you can use a client with POST request:

```
http://localhost:8080/products
```

With JSON body:
```
{
"name": "Banana",
"category": "Fruit",
"price": "2.56"
}
```

To create new order respectfully

```
http://localhost:8080/orders
```


With JSON body:
```
{
"date":"2012-03-27",
"productsIDS": [3, 2, 5],
"status" : "PENDING"
}
```

There is a /vatapi endpoint exposed with which you can calculate VAT of a product.
Use a GET method and pass the following JSON body:
```
{
"id" : "1",
"name": "Banana",
"category": "Fruit",
"price": "5"
}
```

And you get the price with the tax as a JSON
```
{
    "id": 1,
    "name": "Banana",
    "category": "Fruit",
    "price": 6.0
}
```

## Built With

* [Spring](https://spring.io/) - The framework used
* [Gradle](https://gradle.org/) - Build tool

## Authors

* **Stoyan Apostolov** -  [GitLab](https://gitlab.com/stoyan_apostolov)
